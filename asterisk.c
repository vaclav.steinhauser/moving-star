#include <poll.h>
#include <stdio.h>
static int length=50;
static int delay=100;
int main(){
int dir=1;
int pos_of_star=0;
while(1==1){
	poll(NULL, 0, delay);				//wait
	pos_of_star+=dir;				//move star in direction
	if(pos_of_star==0||pos_of_star==length){	//if reacher border
		pos_of_star-=dir;			//take back last move
		dir*=-1;				//reverse direction
		pos_of_star+=dir;			//make a move
	}
	printf("|");					//start of each line
	int i=0;
	while(i<pos_of_star){
		printf(" ");				//spaces before star
		i++;
	}
	printf("*");					//star
	while(i<length){
		printf(" ");				//spaces after star
		i++;
	}
	printf("|\r");					//end of line and return at start
	fflush(stdout);					//print
}
}

